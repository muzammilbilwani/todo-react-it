# todo-react

> React Todo

> [Demo](https://todo-react-it.stackblitz.io/)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm start

# build for production with minification
npm run build
```
