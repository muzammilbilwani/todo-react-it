import React from 'react';
import SingleTodo from './SingleTodo';

export default class Todo extends React.Component {

  static count = 0;

  constructor(props) {
    super();
    this.state = {
      todos: [],
      filtered_todo: [],
      new_todo: {
        title: "",
        priority: "high",
        id: Todo.count
      },
      filterBy: 'all'
    }

    this.handleTodoInput = this.handleTodoInput.bind(this);
    this.add_todo = this.add_todo.bind(this);
    this.delete_todo = this.delete_todo.bind(this);
    this.enterKeyPressed = this.enterKeyPressed.bind(this);
    this.handleTodoPriority = this.handleTodoPriority.bind(this);
    this.filterTodo = this.filterTodo.bind(this);
  }

  handleTodoPriority(event) {
    let todo_copy = this.state.new_todo;
    todo_copy.priority = event.target.value;
    this.setState({
      new_todo: todo_copy
    })
  }

  handleTodoInput(event) {

    let todo_copy = this.state.new_todo;
    todo_copy.title = event.target.value;
    this.setState({
      new_todo: todo_copy
    })
  }

  add_todo() {
    if (this.state.new_todo.title === '')
      return;

    let todos_updated = this.state.todos;

    todos_updated.push(this.state.new_todo);
    Todo.count++;
    this.setState(prevState => ({
      todos: todos_updated,
      new_todo: {
        id: Todo.count,
        title: "",
        priority: "high"
      }
    }))
    this.filterTodo(this.state.filterBy);
  }

  delete_todo(id) {
    console.log(id);

    this.setState((prevState) => ({
      todos: prevState.todos.filter((element) => {
        return element.id !== id;
      }),
      filtered_todo: prevState.filtered_todo.filter((element) => {
        return element.id !== id && (this.state.filterBy === 'all' || element.priority === this.state.filterBy)
      })
    }))
    console.log(this.state.filterBy);


  }

  done_todo(id) {
    this.setState((prevState) => ({
      todos: prevState.todos.map((element, i) => {
        if (element.id === id) {
          element.done = !element.done
        }
        return element;
      })
    }))
  }

  enterKeyPressed(e) {
    if (e.key === 'Enter') {
      this.add_todo();
    }
  }

  filterTodo(type) {
    if (type === 'all') {
      this.setState({
        filterBy: type,
        filtered_todo: this.state.todos
      })
    }
    else {
      this.setState({
        filterBy: type,
        filtered_todo: this.state.todos.filter((item) => { return item.priority === type })
      })
    }
  }

  render() {
    return (
      <div>
        <h1 className="center">Todo App</h1>
        <div className="form">
          <div className="form-group">
            <label className="control-label">
              Title
            </label>
            <input className="form-control todo-input"
              placeholder="New Todo"
              value={this.state.new_todo.title}
              onKeyPress={this.enterKeyPressed}
              onChange={this.handleTodoInput} />
          </div>
          <div className="form-group">
            <label className="control-label">
              Priority
            </label>
            <select className="form-control" onChange={this.handleTodoPriority}>
              <option value="high">High</option>
              <option value="medium">Medium</option>
              <option value="low">Low</option>
            </select>
          </div>
          <button className="btn btn-primary" onClick={this.add_todo}>Add</button>
        </div>
        <hr />
        <div className="filter">
          <button className="btn btn-primary" onClick={() => { this.filterTodo('all') }}>All</button>
          <button className="btn btn-danger" onClick={() => { this.filterTodo('high') }}>High</button>
          <button className="btn btn-success" onClick={() => { this.filterTodo('medium') }}>Medium</button>
          <button className="btn btn-info" onClick={() => { this.filterTodo('low') }}>Low</button>
        </div>
        <div className="todo-list">
          {this.state.filtered_todo.map((item, index) => {
            return <SingleTodo
              todo={item}
              key={item.id}
              delete_todo={() => { this.delete_todo(item.id) }}
              done_todo={() => { this.done_todo(item.id) }}
            />
          })}
        </div>
      </div>
    )
  }

}
