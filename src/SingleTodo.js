import React from 'react';

export default class SingleTodo extends React.Component {

  constructor(props) {
    super();
  }


  render() {
    return (
      <div className={`todo ${this.props.todo.done ? 'done' : ''} animated flipInX ${this.props.todo.priority}`} >
        <p>{this.toUpperCase(this.props.todo.title)}</p>
        <div className="options">
          <i className="fa fa-check-circle" aria-hidden="true" onClick={this.props.done_todo}></i>
          <i className="fa fa-trash" aria-hidden="true" onClick={this.props.delete_todo}></i>
        </div>
      </div>
    )
  }

  toUpperCase(value) {

    return value.toUpperCase();
  }


}